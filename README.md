# Projet de documentation pour le lycée Jules Verne

Les pages générés sont à l'adresse :
[https://yannick-j.gitlab.io/ysphinx](https://yannick-j.gitlab.io/ysphinx)

Les fichiers sont à l'adresse :
[https://gitlab.com/yannick-j/ysphinx/-/artifacts](https://gitlab.com/yannick-j/ysphinx/-/artifacts)
