---
orphan: true
---

# Exemple de fichier avec la syntaxe markdown pour le wiki sphinx

## CommonMark

### Les paragraphes

Ceci est un exemple de longue phrase
sans aucun sens
mais qui sert d'exemple
pour montrer
que le retour à la ligne
n'est pas pris en compte.

Ceci est une deuxième phrase pour servir d'exemple de paragraphe.

### La mise en forme des caractères

Un exemple de mot en *emphase* (italique), en **emphase forte** (gras)
du `code`.

### Les listes

- le premier élément
  sur deux lignes indentées correctement
- le deuxième élément

   - le premier sous-élément précédé d'une ligne vide et indenté par rapport au premier
   - le deuxième sous-élément

1. le premier élément
1. le deuxième élément

   1. le premier sous-élément
   1. le deuxième sous-élément

### Les lignes horizontales

Une ligne horizontale est constituée de `---` précédés d'une ligne vide,

---

... et suivis d'une ligne vide.

### Les liens

Ce paragraphe contient un [lien vers le site jvcergy](https://jvcergy.com/).

### Les images

![Logo MarkDown](img/md-logo.png)

### Citations

> Une citation

### Des blocs de code

```
n = int(input("Entrez un nombre pour calculer sa factorielle"))

if n < 0:
    raise ValueError("le nombre doit être positif")

factorielle= 1
for i in range(2, n + 1):
    factorielle*= i

print(factorielle)
```

## MyST et ses extensions

### Typographie

Ceci est un exemple de longue phrase \
sans aucun sens \
mais qui sert d'exemple \
pour montrer \
que le retour à la ligne \
est pris en compte.

### Mise en forme des caractères

Un exemple de caractères en indice H{sub}`2`O, en exposant T{sup}`ale`.

Du code avec coloration syntaxique du langage `a = "b"`{l=python}.

### Les liens

https://jvcergy.com/

### Les images

![Logo sphinx](img/sphinx-logo.svg){width=50px}

![Logo sphinx](img/sphinx-logo.svg){width=50px align=center}

:::{figure-md}
:width: 50 px
:align: center

![Logo sphinx](img/sphinx-logo.svg)

La légende
:::

## Les tableaux

| A     | not A |
|:-----:|-------|
| False | True  |
| True  | False |

:::{table} Truth table for "not"
:align: center

| A     | not A |
|-------|-------|
| False | True  |
| True  | False |
:::

:::{list-table} list table
:align: center
:widths: 15 10 30
:header-rows: 1

*   - Treat
    - Quantity
    - Description
*   - Albatross
    - 2.99
    - On a stick!
*   - Crunchy Frog
    - 1.49
    - If we took the bones out, it wouldn't be
 crunchy, now would it?
*   - Gannet Ripple
    - 1.99
    - On a stick!
:::

:::{csv-table} csv table
:widths: 15, 10, 30
:header-rows: 1

"Treat", "Quantity", "Description"
"Albatross", 2.99, "On a stick!"
"Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be crunchy, now would it?"
"Gannet Ripple", 1.99, "On a stick!"
:::

## Les formules mathématiques

Équation {math:numref}`célérité2` de la célérité:

```{math}
:label: célérité2
c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}
```

Une formule en ligne {math}`c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}`

\begin{gather*}
a_1=b_1+c_1\\
a_2=b_2+c_2-d_2+e_2
\end{gather*}

\begin{align}
a_{11}& =b_{11}&
  a_{12}& =b_{12}\\
a_{21}& =b_{21}&
  a_{22}& =b_{22}+c_{22}
\end{align}

## Notes en bas de page

Cette ligne contient deux notes[^note] en bas de page[^page].

[^note]: Une note.
[^page]: En bas de page.

## Des blocs de code

:::{code-block} python
:lineno-start: 1
:emphasize-lines: 2,3
:caption: code python pour le calcul d'une factorielle

n = int(input("Entrez un nombre pour calculer sa factorielle"))

if n < 0:
    raise ValueError("le nombre doit être positif")

factorielle= 1
for i in range(2, n + 1):
    factorielle*= i

print(factorielle)
:::

## Les avertissements

:::{tip}
Let's give readers a helpful hint!
:::

:::{attention}
Let's give readers a helpful hint!
:::

:::{caution}
Let's give readers a helpful hint!
:::

:::{danger}
Let's give readers a helpful hint!
:::

:::{error}
Let's give readers a helpful hint!
:::

:::{hint}
Let's give readers a helpful hint!
:::

:::{important}
Let's give readers a helpful hint!
:::

:::{note}
Let's give readers a helpful hint!
:::

:::{seealso}
Let's give readers a helpful hint!
:::

:::{tip}
Let's give readers a helpful hint!
:::

:::{warning}
Let's give readers a helpful hint!
:::

:::{admonition} My custom title with *Markdown*!
This is a custom title for a tip admonition.
:::

:::{note}
:class: dropdown

This admonition has been collapsed,
meaning you can add longer form content here,
without it taking up too much space on the page.
:::

## sphinx-design

:::{card} Card Title
Header
^^^
Card content
+++
Footer
:::

---

::::{tab-set}

:::{tab-item} Label1
Content 1
:::

:::{tab-item} Label2
Content 2
:::

::::

---

```{button-link} https://yannick-j.gitlab.io/ysphinx/pdf/exemple_markdown.pdf
:color: primary
:outline:
{fa}`download` pdf
```

---

[{fa}`download` md](path:exemple_markdown.md)

