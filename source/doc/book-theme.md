# La syntaxe book-theme

## Épigraphe

:::{epigraph}
Here's my quote, it's pretty neat.
I wonder how many lines I can create with
a single stream-of-consciousness quote.
I could try to add a list of ideas to talk about.
I suppose I could just keep going on forever,
but I'll stop here.

-- Jo the Jovyan, *[the jupyter book docs](https://jupyterbook.org)*
:::

## Notes marginales

Here's my sentence and a sidenote[^sn1].

[^sn1]: And here's my sidenote content.

Here's my sentence and a marginnote[^mn1].

[^mn1]: {-} And here's my marginnote content.

---

```{margin} **Here is my margin content**
It is pretty cool!
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nulla felis, imperdiet id neque in, auctor consectetur massa. Fusce id blandit quam. Praesent sed elit quis purus cursus vehicula ultricies volutpat lacus. Maecenas suscipit tincidunt nisl eget euismod. Nam maximus, lectus sed lacinia consectetur, magna elit congue lacus, ac gravida mauris libero vitae orci. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam molestie magna eget luctus rhoncus. Aenean nec lorem placerat, sagittis risus id, mollis augue. Nunc fermentum, libero at tincidunt fringilla, nibh velit ullamcorper leo, sed tincidunt tellus nunc ut est. Nullam suscipit arcu ipsum, id rhoncus ipsum porta eu.

---

```{figure} img/md-logo.png
---
figclass: margin-caption
alt: My figure text
name: myfig5
---
And here is my figure caption, if you look to the left, you can see that COOL is in big red letters. But you probably already noticed that, really I am just taking up space to see how the margin caption looks like when it is really long :-).
```

---

```{figure} img/sphinx-logo.svg
---
figclass: margin
alt: My figure text
name: myfig4
---
And here is my figure caption
```

Fusce at efficitur risus, quis laoreet libero. Nullam et blandit purus. Quisque pellentesque dolor sed pharetra tincidunt. Vestibulum quis eros leo. Morbi at turpis ut urna volutpat lobortis at eu urna. Sed vitae velit bibendum, sagittis magna vitae, gravida erat. Proin suscipit erat a eros vulputate condimentum. Ut magna erat, placerat vitae suscipit ac, aliquet eu lorem. Praesent ac urna vel purus pretium suscipit. Fusce facilisis sem sagittis risus vehicula, ut volutpat ex euismod. Nulla cursus, neque in rutrum hendrerit, massa purus congue nibh, sit amet ornare libero mi faucibus tellus. In mattis venenatis ex nec condimentum. Aenean quam magna, hendrerit at risus ac, vehicula dignissim lorem.

Mauris tincidunt erat et eleifend vehicula. Cras non quam quis arcu pretium lacinia. Sed sit amet lorem blandit, posuere eros non, sagittis diam. Integer scelerisque, quam et pretium consequat, eros leo feugiat orci, in ultricies nunc magna nec libero. Suspendisse facilisis in nisl et vulputate. Cras sed pellentesque ex, sed fermentum nisi. Aliquam vel dapibus sapien. Integer commodo mattis nisl quis finibus. Quisque risus enim, laoreet vitae imperdiet at, blandit et ante. Aenean quis sapien nisi. Donec et elementum urna.

---

:::{note}
:class: margin
This note will be in the margin!
:::

Fusce ac facilisis est, in egestas eros. Integer vel libero nulla. Donec turpis orci, vestibulum sed dui eu, dapibus suscipit ipsum. Vivamus varius mollis nisi eget cursus. Aenean quis rutrum nibh, sed convallis sem. Aliquam a nisl ac risus lacinia gravida. Cras eget dolor elementum, tempor nunc eu, viverra augue. Praesent ultrices urna eget ante auctor, at dignissim urna pretium. Duis in luctus justo.

## Cadre latéral

````{sidebar} **My sidebar title**
Here is my sidebar content, it is pretty cool!
````

Cras ante nunc, rutrum nec tempor eu, tristique sed diam. Phasellus quis auctor velit. Aenean a erat sed lectus congue sodales id vel quam. Vivamus odio diam, tristique at rhoncus nec, interdum et nibh. Cras rutrum tortor eu nibh pellentesque cursus. Aenean luctus in erat ut malesuada. Nulla tempus turpis nec purus finibus, a congue sapien iaculis. Nullam vehicula risus nec velit lobortis condimentum. Ut porta libero non arcu auctor iaculis. Nulla orci est, ultrices quis orci at, pharetra volutpat velit. In hac habitasse platea dictumst. Proin quis eros blandit, sodales tellus non, accumsan augue. Morbi cursus tortor nisl, sed interdum nunc viverra nec. Donec vestibulum massa in odio rutrum, cursus egestas est ultricies. Praesent dui velit, interdum sit amet risus eleifend, mollis pretium odio. Vivamus non vulputate lorem. 
Result

## MyST Markdown elements

Here are a few design elements to show off MyST Markdown.

### Table alignment

To ensure that markdown alignment is rendered properly.

| Default Header | Left Align | Right Align | Center Align |
| -------------- | :--------- | ----------: | :----------: |
| Cell 1         | Cell 2     | Cell 3      | Cell 4       |
| Cell 1         | Cell 2     | Cell 3      | Cell 4       |

### List table width

Testing list tables take width as expected.

```{list-table}
:width: 100%
* - a
  - b
* - c
  - d
```

https://gitlab.com/yannick-j/ysphinx

[https://github.com/pydata/pydata-sphinx-theme/pull/1012](https://github.com/pydata/pydata-sphinx-theme/pull/1012)
