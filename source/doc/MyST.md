# La syntaxe MyST (et ses extensions)

## Paragraphes

Si on veut forcer le retour à la ligne,
on écrit le caractère `\`` en fin de ligne.

````{card}
Le code suivant:
^^^
```md
Ceci est un exemple de longue phrase \
sans aucun sens \
mais qui sert d'exemple \
pour montrer \
que le retour à la ligne \
est pris en compte.
```
````

```{card}
Donne le rendu suivant:
^^^
Ceci est un exemple de longue phrase \
sans aucun sens \
mais qui sert d'exemple \
pour montrer \
que le retour à la ligne \
est pris en compte.
```

---

## Mise en forme des caractères

Le code suivant:

````{card}
Le code suivant:
^^^
```md
Un exemple de caractères en indice H{sub}`2`O, en exposant T{sup}`ale`.

Du code avec coloration syntaxique du langage `a = "b"`{l=python}.
```
````

```{card}
Donne le rendu suivant:
^^^
Un exemple de caractères en indice H{sub}`2`O, en exposant T{sup}`ale`.

Du code avec coloration syntaxique du langage `a = "b"`{l=python}.
````

## Les liens

Un lien simple est directement reconnu:

```md
https://jvcergy.com/
```

Donne le rendu suivant:

----

https://jvcergy.com/

----

Un [lien vers un titre](#les-liens)

Un lien de téléchargement : [](path:MyST.md)

[hyperlien](https://fr.wikipedia.org/wiki/Hyperlien)

## Les images

On modifie la largeur (en pixels) d'une image
avec l'option `width=50px` :

```md
![Logo sphinx](img/sphinx-logo.svg){width=50px}
```

Qui donne : ![Logo sphinx](img/sphinx-logo.svg){width=50px}

On modifie l'alignement avec les options `align=center`, `align=right`.

```md
![Logo sphinx](img/sphinx-logo.svg){width=50px align=center}
```

Qui donne : ![Logo sphinx](img/sphinx-logo.svg){width=50px align=center}

````md
:::{figure-md}
:width: 100 px
:align: center

![Logo sphinx](img/sphinx-logo.svg)

La légende
:::
````

```{figure} img/sphinx-logo.svg
:width: 100 px
:align: center
:name: fig_MyST1

Logo sphinx

La *légende* peut contenir plusieurs lignes.
```

Une référence à la figure {ref}`fig_MyST1`.

## Citations

On peut ajouter un auteur à une citation.

{attribution="Hamlet act 4, Scene 5"}
> We know what we are, but know not what we may be.

## Les tableaux

```{table} Truth table for "not"
:align: center

| A     | not A |
|-------|-------|
| False | True  |
| True  | False |
```

```{list-table} list table
:align: center
:widths: 15 10 30
:header-rows: 1

*   - Treat
    - Quantity
    - Description
*   - Albatross
    - 2.99
    - On a stick!
*   - Crunchy Frog
    - 1.49
    - If we took the bones out, it wouldn't be
 crunchy, now would it?
*   - Gannet Ripple
    - 1.99
    - On a stick!
```

```{csv-table} csv table
:widths: 15, 10, 30
:header-rows: 1

"Treat", "Quantity", "Description"
"Albatross", 2.99, "On a stick!"
"Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be crunchy, now would it?"
"Gannet Ripple", 1.99, "On a stick!"
```

## Les formules mathématiques

Équation {eq}`eq_MyST_1` de la célérité:

```{math}
:label: eq_MyST_1

c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}
```

Une formule en ligne {math}`c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}`

Une formule en ligne $c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}$

\begin{gather*}
a_1=b_1+c_1\\
a_2=b_2+c_2-d_2+e_2
\end{gather*}

\begin{align}
a_{11}& =b_{11}&
  a_{12}& =b_{12}\\
a_{21}& =b_{21}&
  a_{22}& =b_{22}+c_{22}
\end{align}

## Définitions

Le terme à définir
: La définition du terme.

En ajoutant `{.glossary}` à la ligne précédente,
on peut créer une référence au terme.

{.glossary}
Glossary
: A list of often difficult or specialized words with their definitions, often placed at the back of a book.

{term}`Glossary`

## Notes en bas de page

Cette ligne contient deux notes[^note] en bas de page[^page].

[^note]: Une note.
[^page]: En bas de page.

## Des blocs de code

Le code suivant:

````md

```python
n = int(input("Entrez un nombre pour calculer sa factorielle"))

if n < 0:
    raise ValueError("le nobre doit être positif")

factorial = 1
for i in range(2, n + 1):
    factorial *= i

print(factorial)
```

````

Est rendu par:

----

````{code-block} python
:lineno-start: 1
:emphasize-lines: 2,3
:caption: Code python pour le calcul d'une factorielle

n = int(input("Entrez un nombre pour calculer sa factorielle"))

if n < 0:
    raise ValueError("le nombre doit être positif")

factorielle= 1
for i in range(2, n + 1):
    factorielle*= i

print(factorielle)
````

