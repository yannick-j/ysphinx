:orphan: true

***********************************************************************
Exemple de fichier avec la syntaxe ReStructuredText pour le wiki sphinx
***********************************************************************

Les Titres
----------

On souligne par une ligne de même longueur composée des caractères
``=``, ``-`` puis ``^`` selon le niveau de titre. 

Le code est le suivant::

   ********************
   Le titre du document
   ********************

   Un titre de niveau 1
   ====================

   Un titre de niveau 2
   --------------------

   Un titre de niveau 3
   ^^^^^^^^^^^^^^^^^^^^

Les paragraphes
---------------

Les paragraphes sont des blocs de lignes
séparés du reste par des lignes vides.

Le code suivant:

.. code-block:: text

   Ceci est un exemple de longue phrase
   sans aucun sens
   mais qui sert d'exemple
   pour montrer
   que le retour à la ligne
   n'est pas pris en compte.

   Ceci est une deuxième phrase pour servir d'exemple de paragraphe.

Donne le rendu suivant:

----

Ceci est un exemple de longue phrase
sans aucun sens
mais qui sert d'exemple
pour montrer
que le retour à la ligne
n'est pas pris en compte.

Ceci est une deuxième phrase pour servir d'exemple de paragraphe.

----

Si on veut respecter le retour à la ligne,
par exemple pour de la poésie ou une adresse,
on écrit le caractère ``|`` en début de ligne.

Le code suivant:

.. code-block:: text

   | Ceci est un exemple de longue phrase
   | sans aucun sens
   | mais qui sert d'exemple
   | pour montrer
   | que le retour à la ligne
   | est pris en compte.

Donne le rendu suivant:

----

| Ceci est un exemple de longue phrase
| sans aucun sens
| mais qui sert d'exemple
| pour montrer
| que le retour à la ligne
| est pris en compte.

----

La mise en forme des caractères
-------------------------------

Le code suivant::

   Un exemple de mot en *emphase* (italique), en **emphase forte** (gras),
   du ``code``.

Donne le rendu suivant:

----

Un exemple de mot en *emphase* (italique), en **emphase forte** (gras),
du ``code``, en indice the chemical formula for pure water is |H2O|.

.. |H2O| replace:: H\ :sub:`2`\ O

----

Les listes
----------

On commence par une ligne vide, et chaque élément est précédé du caractère ``-``, ``+`` ou ``*``.
On indente

Le code suivant::

   - le premier élément
     sur deux lignes indentées correctement
   - le deuxième élément

      + le premier sous-élément précédé d\'une ligne vide et indenté par rapport au premier
      + le deuxième sous-élément

Donne le rendu suivant:

----

- le premier élément
  sur deux lignes indentées correctement
- le deuxième élément

   - le premier sous-élément précédé d'une ligne vide et indenté par rapport au premier
   - le deuxième sous-élément

----

Pour une liste numérotée, on utilise les numéros suivis du caractère ``.``,
ou les caractère ``#.``, la numérotation est alors automatique.

Le code suivant::

   #. le premier élément
   #. le deuxième élément

      #. le premier sous-élément
      #. le deuxième sous-élément

Donne le rendu suivant:

----

#. le premier élément
#. le deuxième élément

   #. le premier sous-élément
   #. le deuxième sous-élément

----

Les liens
---------

Un lien simple est directement reconnu::

    https://jvcergy.com/

Donne le rendu suivant:

----

https://jvcergy.com/

----


Un lien avec du texte contient le texte
entre ````` suivi du caractère ``_``,
La cible du lien est précisée par le code suivant::

    Ce paragraphe contient un `lien vers le site jv`_.

    .. _lien vers le site jv: https://jvcergy.com/

Qui donne le rendu suivant:

----

Ce paragraphe contient un `lien vers le site jv`_.

----

.. _lien vers le site jv: https://jvcergy.com/

Les images et les figures
-------------------------

Pour inclure une image
(au format ``svg``, ``png``, ``gif`` ou ``jpeg``),
il faut préciser le chemin de l'image,
``img/sphinx-logo.svg`` si le fichier ``sphinx-logo.svg``
est dans un dossier ``img``,
avec le code suivant::

   .. image:: img/sphinx-logo.svg

Qui donne :

----

.. image:: img/sphinx-logo.svg

----

On modifie la largeur (en pixels) avec l'option ``:width: 50 px``,
l'alignement avec l'option ``:align: center``, ``:align: right``,
en indentant bien au niveau du mot ``image``::

   .. image:: img/sphinx-logo.svg
      :width: 50 px
      :align: center

----

.. image:: img/sphinx-logo.svg
   :width: 50 px
   :align: center

----

On peut inclure une image en ligne par une substitution::

    Cette ligne contient un logo |logo|.

----

Cette ligne contient un logo |logo|.

----

.. |logo| image:: img/sphinx-logo.svg
   :width: 20 px

Une figure peut être légendée::

    .. figure:: img/sphinx-logo.svg
    :width: 50 px
       :align: center

       Le titre de la figure

       La légende peut comporter plusieurs lignes.

----

.. figure:: img/sphinx-logo.svg
   :width: 50 px
   :align: center

   Le titre de la figure

   La légende peut comporter plusieurs lignes.

----

Les tableaux
------------
.. table:: Truth table for "not"
   :widths: auto
   :align: center

   =====  =====
     A    not A
   =====  =====
   False  True
   True   False
   =====  =====

=====  =====  ======
   Inputs     Output
------------  ------
  A      B    A or B
=====  =====  ======
False  False  False
True   False  True
False  True   True
True   True   True
=====  =====  ======

+------------------------+------------+----------+----------+
| Header row, column 1   | Header 2   | Header 3 | Header 4 |
| (header rows optional) |            |          |          |
+========================+============+==========+==========+
| body row 1, column 1   | column 2   | column 3 | column 4 |
+------------------------+------------+----------+----------+
| body row 2             | Cells may span columns.          |
+------------------------+------------+---------------------+
| body row 3             | Cells may  | - Table cells       |
+------------------------+ span rows. | - contain           |
| body row 4             |            | - body elements.    |
+------------------------+------------+---------------------+

.. csv-table:: Frozen Delights!
   :header: "Treat", "Quantity", "Description"
   :widths: 15, 10, 30

   "Albatross", 2.99, "On a stick!"
   "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be
   crunchy, now would it?"
   "Gannet Ripple", 1.99, "On a stick!"

Les formules mathématiques
--------------------------

.. _LaTeX: https://docutils.sourceforge.io/docs/ref/rst/mathematics.html

Les formules mathématiques sont écrites en format LaTeX_::

    .. math::

       c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}

----

.. math::

   c = \frac{1}{\sqrt{\varepsilon_0 \mu_0}}

----

Les caractères unicode sont aussi interprétés::

    .. math::

       c = \frac{1}{\sqrt{ε_0 μ_0}}

----

.. math::
   :label: celerite

   c = \frac{1}{\sqrt{ε_0 μ_0}}

La vitesse de propagation de l'onde électro-magnétique
:math:`c` est donnée par l'équation :eq:`celerite`.

----

Pour une formule en ligne::

    Une formule en ligne: :math:`c = \frac{1}{\sqrt{ε_0 μ_0}}`.

----

Une formule en ligne: :math:`c = \frac{1}{\sqrt{ε_0 μ_0}}`.

----

Notes en bas de page et citations
---------------------------------

Cette ligne contient deux notes [#note]_ en bas de page [#page]_
et une citation: [citation2]_.

.. [#note] Une note.
.. [#page] En bas de page.

.. [citation2] Le texte de la première citation.

Des blocs de code
-----------------

Le code suivant::

    .. code-block:: python

        n = int(input("Entrez un nombre pour calculer sa factorielle"))

        if n < 0:
            raise ValueError("le nobre doit être positif")

        factorial = 1
        for i in range(2, n + 1):
            factorial *= i

        print(factorial)

Est rendu par:

----

.. code-block:: python

    n = int(input("Entrez un nombre pour calculer sa factorielle"))

    if n < 0:
        raise ValueError("le nobre doit être positif")

    factorial = 1
    for i in range(2, n + 1):
        factorial *= i

    print(factorial)

----

`Introduction à reStructuredText en français sur le site de l'AFUL`_

`Documentation officielle en anglais sur le site de sphinx-doc`_

.. _Introduction à reStructuredText en français sur le site de l'AFUL: https://aful.org/wikis/interop/ReStructuredText
.. _Documentation officielle en anglais sur le site de sphinx-doc: https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html

