# La syntaxe CommonMark

## Les Titres

On écrit un titre en le précédant d'un ou plusieurs caractères `#`.

Le code suivant :

```md
# Le titre du document

## Un titre de niveau 1

### Un titre de niveau 2

#### Un titre de niveau 3
```

Donne le rendu suivant :

---

:::
# Le titre du document

## Un titre de niveau 1

### Un titre de niveau 2

#### Un titre de niveau 3
:::

---

## Les paragraphes

Les paragraphes sont des blocs de lignes
séparés du reste par des lignes vides.

:::{card}
Le code suivant:
^^^
```md
Ceci est un exemple de longue phrase
sans aucun sens
mais qui sert d'exemple
pour montrer
que le retour à la ligne
n'est pas pris en compte.

Ceci est une deuxième phrase pour servir d'exemple de paragraphe.
```
:::

:::{card}
Donne le rendu suivant:
^^^
Ceci est un exemple de longue phrase
sans aucun sens
mais qui sert d'exemple
pour montrer
que le retour à la ligne
n'est pas pris en compte.

Ceci est une deuxième phrase pour servir d'exemple de paragraphe.
:::

## La mise en forme des caractères

Le code suivant:

```md
Un exemple de mot en *emphase* (italique), en **emphase forte** (gras),
du `code`.
```

Donne le rendu suivant:

----

Un exemple de mot en *emphase* (italique), en **emphase forte** (gras)
du `code`.

----

## Les listes

On commence par une ligne vide,
et chaque élément est précédé du caractère `-`, `+` ou `*`.
On indente

Le code suivant:

```md
- le premier élément
  sur deux lignes indentées correctement
- le deuxième élément

   + le premier sous-élément précédé d'une ligne vide et indenté par rapport au premier
   + le deuxième sous-élément
```

Donne le rendu suivant:

----

- le premier élément
  sur deux lignes indentées correctement
- le deuxième élément

   - le premier sous-élément précédé d'une ligne vide et indenté par rapport au premier
   - le deuxième sous-élément

----

Pour une liste numérotée, on utilise n'importe quel numéro
suivi du caractère `.`,
la numérotation est alors automatique.

Le code suivant:

```md
1. le premier élément
1. le deuxième élément

   1. le premier sous-élément
   1. le deuxième sous-élément
```

Donne le rendu suivant:

----

1. le premier élément
1. le deuxième élément

   1. le premier sous-élément
   1. le deuxième sous-élément

----

## Les lignes horizontales

Une ligne horizontale est constituée de `---` précédés et suivis
d'une ligne vide.

---

## Les liens

Un lien contient du texte optionnel entre `[]`,
suivi de l'url entre `()`,

```md
[lien vers le site jvcergy](https://jvcergy.com/)
```

Donne le rendu suivant:

----

Ce paragraphe contient un [lien vers le site jvcergy](https://jvcergy.com/).

## Les images

Pour inclure une image
(au format `svg`, `png`, `gif` ou `jpeg`),
il faut préciser le chemin de l'image,
`img/md-logo.png` si le fichier `md-logo.png`
est dans un dossier `img`,
avec le code suivant:

```md
![Logo MarkDown](img/md-logo.png)
```

Le texte entre `[]` est le texte alternatif,
qui s'affiche si l'image est manquante ou pour les malvoyants.

Qui donne :

----

![Logo MarkDown](img/md-logo.png)

----

## Citations

Une citation est une ligne précédée du caractére `>`

```md
> Une citation
```

> Une citation

## Des blocs de code

Le code suivant:

:::{code-block}
```
n = int(input("Entrez un nombre pour calculer sa factorielle"))

if n < 0:
    raise ValueError("le nobre doit être positif")

factorial = 1
for i in range(2, n + 1):
    factorial *= i

print(factorial)
```
:::

Est rendu par:

----

```
n = int(input("Entrez un nombre pour calculer sa factorielle"))

if n < 0:
    raise ValueError("le nombre doit être positif")

factorielle= 1
for i in range(2, n + 1):
    factorielle*= i

print(factorielle)
```

