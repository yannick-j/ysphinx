# La syntaxe de l'extension sphinx-design

## Grids

### Introduction

Grids are based on a 12 column system, which can adapt to the size of the viewing screen.

A grid directive can be set with the number of default columns (1 to 12); either a single number for all screen sizes, or four numbers for extra-small (<576px), small (768px), medium (992px) and large screens (>1200px), then child `grid-item` directives should be set for each item.

Try re-sizing the screen to see the number of columns change:

````{grid} 1 2 3 4
:outline:

```{grid-item}
A
```

```{grid-item}
B
```

```{grid-item}
C
```

```{grid-item}
D
```
````

You can also use the `auto` value for the column width(s), to size columns based on the natural width of their content.

### Controlling spacing between items

You can set the spacing between grid items with the `gutter` option. Like for grid columns, you can either provide a single number or four for small, medium and large and extra-large screens.

````{grid} 2
:gutter: 1

```{grid-item-card}
A
```

```{grid-item-card}
B
```
````

````{grid} 2
:gutter: 3 3 4 5

```{grid-item-card}
A
```

```{grid-item-card}
B
```
````

### Item level column width

You can override the number of columns a single item takes up by using the `columns` option of the `grid-item` directive. Given the total columns are 12, this means 12 would indicate a single item takes up the entire grid row, or 6 half. Alternatively, use `auto` to automatically decide how many columns to use based on the item content. Like for grid columns, you can either provide a single number or four for small, medium and large and extra-large screens.

````{grid} 2
```{grid-item-card}
:columns: auto

A
```

```{grid-item-card}
:columns: 12 6 6 6

B
```

```{grid-item-card}
:columns: 12

C
```
````

### Reversing the item order

Use the `grid` directive’s `reverse` option to reverse the order of the items. This can be useful if you want an item to be on the right side on large screens, but at the top on small screens.

````{grid} 2
:outline:
:reverse:

```{grid-item}
A
```

```{grid-item}
B
```
````

### Nesting grids

Grids can be nested in other grids to create complex, adaptive layouts.

``````{grid} 1 1 2 2
:gutter: 1

`````{grid-item}

````{grid} 1 1 1 1
:gutter: 1

```{grid-item-card} Item 1.1

Multi-line

content

```

```{grid-item-card} Item 1.2

Content

```

````

`````

`````{grid-item}

````{grid} 1 1 1 1
:gutter: 1

```{grid-item-card} Item 2.1

Content

```

```{grid-item-card} Item 2.2

Content

```

```{grid-item-card} Item 2.3

Content

```
````
`````
``````

### grid options

gutter
: Spacing between items.
One or four integers (for “xs sm md lg”) between 0 and 5.

margin
: Outer margin of grid. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5, auto.

padding
: Inner padding of grid. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5.

outline
: Create a border around the grid.

reverse
: Reverse the order of the grid items.

class-container
: Additional CSS classes for the grid container element.

class-row
: Additional CSS classes for the grid row element.

### grid-item options

columns
: The number of columns (out of 12) a grid-item will take up. One or four integers (for “xs sm md lg”) between 1 and 12 (or auto to adapt to the content).

margin
: Outer margin of grid item. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5, auto.

padding
: Inner padding of grid item. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5.

child-direction
: Direction of children in the grid item: column (default) or row.

child-align
: Alignment of children, along the child-direction: start (default), end, center, justify or spaced.

outline
: Create a border around the grid item.

class
: Additional CSS classes for the grid item element.

## Cards

Cards contain content and actions about a single subject. A card is a flexible and extensible content container, it can be formatted with components including headers and footers, titles and images.

```{card} Card Title

Card content
```

All content before the first occurrence of three or more `^^^` is considered as a header, and all content after the final occurrence of three or more `+++` is considered as a footer.

```{card} Card Title
Header
^^^
Card content
+++
Footer
```

### Card images

You can also add an image as the background of a card or at the top/bottom of the card, with the `img-background`, `img-top`, `img-bottom options`.

### Placing a card in a grid

The `grid-item-card` directive is a short-hand for placing a card content container inside a grid item (see Cards). Most of the card directive’s options can be used also here.

````{grid} 2
```{grid-item-card}  Title 1
A
```

```{grid-item-card}  Title 2
B
```
````

````{grid} 2
:gutter: 1

```{grid-item-card}
A
```

```{grid-item-card}
B
```
````

````{grid} 2
:gutter: 3 3 4 5

```{grid-item-card}
A
```

```{grid-item-card}
B
```
````

````{grid} 2

```{grid-item-card}
:columns: auto

A
```

```{grid-item-card}
:columns: 12 6 6 6

B
```

```{grid-item-card}
:columns: 12

C
```
````

### grid-item-card options

columns
: The number of columns (out of 12) a grid-item will take up. One or four integers (for “xs sm md lg”) between 1 and 12 (or auto to adapt to the content).

margin
: Outer margin of grid item. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5, auto.

padding
: Inner padding of grid item. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5.

class-item
: Additional CSS classes for the grid item element.

Plus all options from card options.

### Clickable cards

Using the link and link-type options, you can turn an entire card into a clickable link.

(cards-clickable)=
## Cards Clickable

```{card} Clickable Card (external)
:link: https://example.com

The entire card can be clicked to navigate to <https://example.com>.
```

```{card} Clickable Card (internal)
:link: cards-clickable
:link-type: ref

The entire card can be clicked to navigate to the `cards` reference target.
```

### Aligning cards

You can use the text-align option to align the text in a card, and also auto margins to align the cards horizontally.

### Card carousels

The card-carousel directive can be used to create a single row of fixed width, scrollable cards. The argument should be a number between 1 and 12, to denote the number of cards to display.

When scrolling a carousel, the scroll will snap to the start of the nearest card.
````{card-carousel} 2

```{card} card 1
content
```

```{card} card 2
Longer

content
```

```{card} card 3
```

```{card} card 4
```

```{card} card 5
```

```{card} card 6
```
````

### card options

width
: The width that the card should take up (in %): auto, 25%, 50%, 75%, 100%.

margin
: Outer margin of grid. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5, auto.

text-align
: Default horizontal text alignment: left, right, center or justify

img-background
: A URI (relative file path or URL) to an image to be placed below the content.

img-top
: A URI (relative file path or URL) to an image to be placed above the content.

img-bottom
: A URI (relative file path or URL) to an image to be placed below the content.

img-alt
: Alternative text for the image (that will be used by screen-readers).

link
: Turn the entire card into a clickable link to an external/internal target.

link-type
: Type of link: url (default), ref, doc, any.

link-alt
: Alternative text for the link (that will be used by screen-readers).

shadow
: The size of the shadow below the card: none, sm (default), md, lg.

class-card
: Additional CSS classes for the card container element.

class-header
: Additional CSS classes for the header element.

class-body
: Additional CSS classes for the body element.

class-title
: Additional CSS classes for the title element.

class-footer
: Additional CSS classes for the footer element.

class-img-top
: Additional CSS classes for the top image (if present).

class-img-bottom
: Additional CSS classes for the bottom image (if present).

## Dropdowns

Dropdowns can be used to toggle, usually non-essential, content and show it only when a user clicks on the header panel.

The dropdown can have a title, as the directive argument, and the open option can be used to initialise the dropdown in the open state.

```{dropdown}
Dropdown content
```

```{dropdown} Dropdown title
Dropdown content
```

```{dropdown} Open dropdown
:open:

Dropdown content
```

### Dropdown opening animations

Use `:animate: fade-in` or `:animate: fade-in-slide-down` options to animate the reveal of the hidden content.

```{dropdown} fade-in
:animate: fade-in

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed iaculis arcu vitae odio gravida congue. Donec porttitor ac risus et condimentum. Phasellus bibendum ac risus a sollicitudin. Proin pulvinar risus ac mauris aliquet fermentum et varius nisi. Etiam sit amet metus ac ipsum placerat congue semper non diam. Nunc luctus tincidunt ipsum id eleifend. Ut sed faucibus ipsum. Aliquam maximus dictum posuere. Nunc vitae libero nec enim tempus euismod. Aliquam sed lectus ac nisl sollicitudin ultricies id at neque. Aliquam fringilla odio vitae lorem ornare, sit amet scelerisque orci fringilla. Nam sed arcu dignissim, ultrices quam sit amet, commodo ipsum. Etiam quis nunc at ligula tincidunt eleifend.
```

```{dropdown} fade-in-slide-down
:animate: fade-in-slide-down

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed iaculis arcu vitae odio gravida congue. Donec porttitor ac risus et condimentum. Phasellus bibendum ac risus a sollicitudin. Proin pulvinar risus ac mauris aliquet fermentum et varius nisi. Etiam sit amet metus ac ipsum placerat congue semper non diam. Nunc luctus tincidunt ipsum id eleifend. Ut sed faucibus ipsum. Aliquam maximus dictum posuere. Nunc vitae libero nec enim tempus euismod. Aliquam sed lectus ac nisl sollicitudin ultricies id at neque. Aliquam fringilla odio vitae lorem ornare, sit amet scelerisque orci fringilla. Nam sed arcu dignissim, ultrices quam sit amet, commodo ipsum. Etiam quis nunc at ligula tincidunt eleifend.
```

### Dropdowns in other components

Dropdowns can be nested inside other components, such as inside parent dropdowns or within grid items.

### dropdown options

open
: Open the dropdown by default.

color
: Set the color of the dropdown header (background and font). One of the semantic color names: primary, secondary, success, danger, warning, info, light, dark, muted.

icon
: Set an octicon icon to prefix the dropdown header.

animate
: Animate the dropdown opening (fade-in or fade-in-slide-down).

margin
: Outer margin of grid. One (all) or four (top bottom left right) values from: 0, 1, 2, 3, 4, 5, auto.

name
: Set a reference-able name for the dropdown container.

class-container
: Additional CSS classes for the container element.

class-title
: Additional CSS classes for the title element.

class-body
: Additional CSS classes for the body element.

## Tabs

Tabs organize and allow navigation between groups of content that are related and at the same level of hierarchy. Each tab should contain content that is distinct from other tabs in a set.

````{tab-set}

```{tab-item} Label1
Content 1
```

```{tab-item} Label2
Content 2
```

````

### Synchronised Tabs

Use the sync option to synchronise the selected tab items across multiple tab-sets. Note, synchronisation requires that JavaScript is enabled.

````{tab-set}

:::{tab-item} Label1
:sync: key1

Content 1
:::

:::{tab-item} Label2
:sync: key2

Content 2
:::

````

````{tab-set}

```{tab-item} Label1
:sync: key1

Content 1
```

```{tab-item} Label2
:sync: key2

Content 2
```

````

### Tabbed code examples

The `tab-set-code` directive provides a shorthand for synced code examples. You can place any directives in a `tab-set-code` that produce a literal_block node with a language attribute, for example code, `code-block` and `literalinclude`. Tabs will be labelled and synchronised by the `language` attribute (in upper-case).

````{tab-set-code}

```{code-block} javascript
a = 1;
```

````

### Tabs in other components

Tabs can be nested inside other components, such as inside dropdowns or within grid items.

### tab-set options

class
: Additional CSS classes for the container element.

### tab-item options

selected
: a flag indicating whether the tab should be selected by default.

sync
: A key that is used to sync the selected tab across multiple tab-sets.

name
: Set a reference-able name for the dropdown container.

class-container
: Additional CSS classes for the container element.

class-label
: Additional CSS classes for the label element.

class-content
: Additional CSS classes for the content element.

