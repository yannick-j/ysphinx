Alphabet grec
=============

En sciences physiques, on a l’habitude de désigner chaque grandeur
physique mesurable et chaque constante par une lettre toujours identique
afin de faciliter la lecture des calculs littéraux.

L’alphabet latin (majuscules et minuscules) ne suffisant pas, on a
ensuite recours à l’alphabet grec, du moins pour les graphies
différentes de l’alphabet latin. On n’utilise par exemple pas l’alpha
majuscule mais plutôt la forme minuscule.

Voici pour vous aider une fiche récapitulative, elle sera apprise au fur
et à mesure des besoins.

+-----------+-----------+---------+-------------+
| Lettre    | Lettre    | Nom     | Lettre      |
| grecque   | grecque   |         | latine      |
| majuscule | minuscule |         | équivalente |
+===========+===========+=========+=============+
|           | α         | alpha   | a           |
+-----------+-----------+---------+-------------+
|           | β         | bêta    | b           |
+-----------+-----------+---------+-------------+
| Γ         | γ         | gamma   | g           |
+-----------+-----------+---------+-------------+
| Δ         | δ         | delta   | d           |
+-----------+-----------+---------+-------------+
|           | ε         | epsilon | e           |
+-----------+-----------+---------+-------------+
|           | η         | êta     | ê           |
+-----------+-----------+---------+-------------+
| Θ         | θ         | thêta   | th          |
+-----------+-----------+---------+-------------+
| Λ         | λ         | lambda  | l           |
+-----------+-----------+---------+-------------+
|           | μ         | mu      | m           |
+-----------+-----------+---------+-------------+
|           | ν         | nu      | n           |
+-----------+-----------+---------+-------------+
| Π         | π         | pi      | p           |
+-----------+-----------+---------+-------------+
|           | ρ         | rhô     | r           |
+-----------+-----------+---------+-------------+
| Σ         | σ         | sigma   | s           |
+-----------+-----------+---------+-------------+
|           | τ         | tau     | t           |
+-----------+-----------+---------+-------------+
| Φ         | φ         | phi     | ph          |
+-----------+-----------+---------+-------------+
|           | χ         | chi     | kh          |
+-----------+-----------+---------+-------------+
| Ψ         | ψ         | psi     | ps          |
+-----------+-----------+---------+-------------+
| Ω         | ω         | omega   | ô           |
+-----------+-----------+---------+-------------+

