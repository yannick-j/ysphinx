Fiche méthodes 3 : Comment rédiger un compte-rendu de démarche d’investigation ou de TP?
========================================================================================

**A sortir à chaque fois que vous rédiger un compte rendu.**

Le succès d’un bon compte-rendu réside dans la rédaction de phrases
courtes. Dans ce compte-rendu je dois utiliser correctement les
connecteurs logiques (donc, car, si, alors…) et le vocabulaire
scientifique adapté.

.. card:: Nom ­ Prénom / classe Date

   **Titre de l’activité ou du TP**

   -  Je décris le problème posé ou je note le but de l’activité (ou du
      TP)

A quelle question dois-je répondre? Quel est l’objectif à atteindre?

.. card::

   -  Je formule des hypothèses et/ou je propose un protocole possible

Je formule des hypothèses argumentées « je pense que… », « à mon
avis… »:

Il s’agit de proposer une ou plusieurs explication permettant de
répondre à la question posée.

J’explique l’expérience que je veux réaliser et qui va me permettre
de valider ou de réfuter mes hypothèses:

il s’agit de décrire dans les grandes lignes ce que je vais faire,
éventuellement à l’aide de schémas soignés, légendés et annotés; de
préciser le matériel utilisé ainsi que les précautions éventuelles à
prendre; je précise les grandeurs mesurées et les paramètres
éventuellement modifiés.

Remarque:
  Dans le cas où le protocole est imposé, faire référence à
  celui-ci.

.. card::

   -  Je note mes observations, je joins mes résultats de mesure

Je présente les observations faites sous forme de schémas légendés
et/ou de commentaires… « J’observe que… » (Il s’agit d’écrire une
phrase par observation).

Je présente les résultats expérimentaux (les mesures accompagnées de
leurs unités et écrites en respectant le nombre de chiffre
significatifs) sous une forme appropriée: tableau de valeur,
représentation graphique (en faisant figurer les grandeurs et les
unités des axes).

.. card::

   -  J’analyse les résultats des observations ou des mesures pour
      répondre à la question posée.

Parmi les observations faites, je regarde celle qui va me permettre
de valider ou d’infirmer l’hypothèse. « D’après mes observations, je
peux déduire que… », « Je constate que… »

Remarques:
  Penser à tenir compte des incertitudes liées aux mesures; mettre
  en évidence les facteurs d’influence, une relation entre les
  grandeurs…; effectuer des calculs si nécessaire. Lorsque
  l’exploitation est guidée, répondre aux questions.

.. card::

   -  Je rédige une conclusion:

La phrase de conclusion doit donner la réponse à la question posée en
validant l’hypothèse formulée.

Si l’hypothèse est infirmée, il faut généralement avoir recours à
d’autres hypothèses qui devront à leur tour être testées. Quand cela
est possible, comparer les résultats obtenus à ceux de la
littérature.
