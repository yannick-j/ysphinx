Fiche méthodes 1 : Notation scientifique, chiffres significatifs et arrondir un résultat
========================================================================================

Écrire un nombre en notation scientifique
-----------------------------------------

.. admonition:: Définition
   :class: note

   Toute mesure ou résultat scientifique doit être écrit en notation
   scientifique de la forme :math:`a \times 10^n`, pour laquelle :math:`a`
   est un nombre décimal n’ayant qu’un seul chiffre différent de zéro
   devant la virgule et :math:`n` est un entier positif ou négatif.

.. admonition:: Attention
   :class: caution

   Attention: l’afficheur de la calculatrice ne présente pas la notation
   scientifique de la même façon; il faut donc savoir faire la
   transposition d’une forme à l’autre.

Exemples:
   - :math:`280~\text{km}` s’écrit :math:`2{,}80 \cdot 10^2 ~\text{km}` affichage en mode scientifique: ``2.80 E2``

   - :math:`0,065~\text{cm}` s’écrit :math:`6,5 \cdot 10^{-2} ~\text{cm}` affichage en mode scientifique: ``6.5 E-2``

.. admonition:: Rappels
   :class: tip

   Multiplication: :math:`10^x \times 10^y = 10^{x + y}`

   et division: :math:`\displaystyle \frac{10^x}{10^y} = 10^{x-y}`

Méthode simple pour déterminer le nombre de chiffres significatifs (c.s.)
-------------------------------------------------------------------------

Pour déterminez le nombre de chiffre significatif d’un nombre, deux cas
se présentent:

.. admonition:: Cas 1
   :class: note

   Le nombre en question commence par **un chiffre non nul**.
   Le nombre de chiffre significatif est égal
   au nombre de chiffre qui constituent le nombre.

Exemples:
    :math:`137` comporte **trois** chiffres significatifs.

    :math:`9,547` comporte **quatre** chiffres significatifs.

.. admonition:: Cas 2
   :class: note

   Le nombre en question commence par un zéro,
   il est donc inférieur à un.
   Il faut alors écrire le nombre en notation scientifique.
   Puis en déduire le nombre de chiffre significatif
   de la même façon que dans le cas 1.

Exemples:
   - :math:`0,37` s’écrit en notation scientifique :math:`3,7 \cdot 10^{-1}`, qui comporte **deux** c.s.

   - :math:`0,00528` s’écrit en notation scientifique :math:`5,28 \cdot 10^{-3}`, qui comporte **trois** c.s.


.. admonition:: Attention
   :class: caution

   Les zéro après la virgule qui sont des chiffres significatifs
   doivent être écrits car ils indiquent un résultat plus précis.

Exemple:
    :math:`100~\text{mL}` s'écrit en notation scientifique
    :math:`1,00 \cdot 10^2~\text{mL}`, qui comporte trois c.s.

Arrondir un résultat
--------------------

.. admonition:: Règle 1
   :class: note

   Lors d’une addition ou d’une soustraction de grandeurs physiques,
   le résultat ne doit pas comporter plus de chiffres après la virgule
   que la grandeur qui en comporte le moins.

Exemples:
    - :math:`11,2~\text{m} + 9,21~\text{m} = 20,41~\text{m}`

    :math:`20,41` doit être arrondi à **un chiffre après la virgule**,
    soit :math:`20,4~\text{m}`, puisque :math:`11,2` ne comporte qu’un
    seul chiffre après la virgule alors que :math:`9,21` en comporte
    deux.

    - :math:`968,43~\text{m} - 7,2~\text{m} = 961,23~\text{m}`

    :math:`961,23` doit également être arrondi à
    **un chiffre après la virgule** soit :math:`961,2~\text{m}`,
    puisque :math:`7,2` ne comporte qu’un seul chiffre après la virgule.

.. admonition:: Règle 2
   :class: note

   Pour les multiplications ou divisions de grandeurs physiques,
   il faut d’abord convertir les grandeurs en notation scientifique.
   Le résultat final ne doit pas avoir plus de chiffres significatifs que
   la grandeur qui en a le moins.

Exemples:
    - :math:`2,5~\text{m} \times 1,53~\text{m} = 3,825~\text{m} \times \text{m}` (soit :math:`\text{m}^2`)

    doit être arrondi à :math:`3,8~\text{m}^2` 
    car :math:`2,5` **n’a que deux chiffres significatifs**
    alors que :math:`1,53` en a trois.

    - :math:`\displaystyle \frac{1,20~\text{dm}^3}{0,010~\text{dm}} = 120~\text{dm}^2`

    :math:`120~\text{dm}^2` doit être arrondi à
    :math:`1,2 \cdot 10^2~\text{dm}^2` car
    :math:`0,010` **n'a que deux chiffres significatifs**
    alors que :math:`1,20` en a trois.

