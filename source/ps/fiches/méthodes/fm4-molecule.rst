Fiche méthode 4 : Comment représenter une molécule?
===================================================

Formule brute
-------------

Règles de représentation
~~~~~~~~~~~~~~~~~~~~~~~~

Les atomes constituant la molécule sont représentés par leur symbole.
Leur nombre est indiqué en indice, à droite du symbole.

Exemple
~~~~~~~

.. figure:: fig/brut.svg
   :alt: Formule brute de l’acide éthanoïque
   :align: center

   Formule brute de l’acide éthanoïque

--------------

Formule développée
------------------

Règles de représentation
~~~~~~~~~~~~~~~~~~~~~~~~

Dans une formule développée,

-  tous les atomes sont représentés par leur symbole et chaque liaison
   est indiquée par un trait entre les atomes. Une liaison multiple est
   indiquée par plusieurs traits parallèles.

-  Lorsqu’une géométrie plane triangulaire existe autour de certains
   atomes, elle est très souvent représentée.

Exemple
~~~~~~~

.. figure:: fig/dev.svg
   :alt: Formule développée de l’acide éthanoïque
   :align: center

   Formule développée de l’acide éthanoïque

--------------

Formule semi-développée
-----------------------

Règles de représentation
~~~~~~~~~~~~~~~~~~~~~~~~

Cette représentation se déduit de la formule développée. Toutes les
liaisons entre les atomes sont représentées sauf celles qui impliquent
un atome d’hydrogène. Les symboles des atomes d’hydrogène figurent à
côté du symbole de l’atome auquel ils sont liés.

Exemple
~~~~~~~

.. figure:: fig/semi-dev.svg
   :alt: Formule semi-développée de l’acide éthanoïque
   :align: center

   Formule semi-développée de l’acide éthanoïque

--------------

Représentation de Lewis
-----------------------

Règles de représentation
~~~~~~~~~~~~~~~~~~~~~~~~

Sur cette représentation, on fait apparaître:

-  tous les doublets liants entre les atomes de la molécule;

-  les doublets non liants de tous les atomes qui en possèdent.

Exemple
~~~~~~~

.. figure:: fig/lewis.svg
   :alt: Formule développée de l’acide éthanoïque
   :align: center

   Formule développée de l’acide éthanoïque

--------------

Écriture topologique
--------------------

Règles de représentation
~~~~~~~~~~~~~~~~~~~~~~~~

-  Une liaison est représentée par un trait, la chaîne carboné par une
   ligne brisée comportant autant de traits que de liaisons .

-  Les atomes de carbone et les atomes d’hydrogène qu’ils portent ne
   sont pas représentés.

-  Les liaisons simples, doubles ou triples sont représentés par un,
   deux ou trois tirets (; ; ).

Exemple
~~~~~~~

.. figure:: fig/topo-acid-ethan.svg
   :alt: Représentation topologique de l’acide éthanoïque
   :align: center

   Représentation topologique de l’acide éthanoïque

--------------

Méthode pour déterminer la représentation de Lewis d’une molécule
-----------------------------------------------------------------

**Exemple:** Déterminons la représentation de Lewis de la molécule de
méthanol:

1. Chercher le numéro atomique de chaque atome, puis écrire leur
   structure électronique.

   **Exemple:** :math:`\text{H} (Z=1): K^1`;
   :math:`\text{C} (Z=6): K^2L^4`; :math:`\text{O} (Z=8): K^2L^6`.

2. Déterminer le nombre de doublets liants et non liants de chaque
   atome.

   -  Le nombre de doublet liant d’un atome est égale au nombre
      d’électron qu’il lui manque pour satisfaire à la règle du duet
      pour l’atome H ou la règle de l’octet pour les autres atomes.

   -  Le nombre de doublet non liant d’un atome est égale au nombre
      d’électrons externes non engagés dans un doublet liant, divisé par
      deux.

   **Exemple:** H possède un doublet liant (règle du duet) C possède
   quatre doublets liants (règle de l’octet) O possède deux doublets
   liants et deux doublets non liants (règle de l’octet).

3. Dessiner les atomes reliés entre eux de telle sorte que chaque atome
   forme le bon nombre de liaison.

   **Exemple:**

.. figure:: fig/lewis-methanol-1.svg
   :alt: Représentation de Lewis du méthanol : 1ère étape
   :align: center

   Représentation de Lewis du méthanol : 1ère étape

4. Compléter le schéma avec les doublets non liants des atomes qui en
   possèdent.

   **Exemple:**

.. figure:: fig/lewis-methanol-2.svg
   :alt: Représentation de Lewis du méthanol : 2ème étape
   :align: center

   Représentation de Lewis du méthanol : 2ème étape

5. Vérifier que les atomes d’hydrogène respectent la règle du duet (ils
   sont donc entourés d’un seul doublet) et que les autres atomes
   respectent la règle de l’octet (ils sont donc entourés de quatre
   doublets).

   **Exemple:** Les atomes C et O sont entourés de quatre doublets et
   les atomes H sont entourés d’un seul doublet.

