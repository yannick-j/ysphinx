Fiche méthode 5 : Construire un tableau d’avancement
====================================================

Construire un tableau d’avancement
----------------------------------

.. figure:: fig/tab-av.svg
   :alt: Tableau d'avancement de réaction chimique
   :align: center
   :target: ../../../_images/tab-av.svg

   Tableau d'avancement de réaction chimique

Avec: :math:`n` qui correspond à une quantité de matière (en mol);
:math:`i` pour initial et :math:`f` pour final.

Remarque: on ne fait pas apparaître le nombre stœchiométrique lorsqu’il
est égal à :math:`1`.

-  Sur la ligne :math:`\ell_1` on écrit l’équation chimique et on
   l’équilibre;

-  sur la ligne :math:`\ell_2` on indique les noms des différentes
   grandeurs exprimées dans le tableau;

-  la ligne :math:`\ell_3` correspond à l’état initial (E.I.). La
   transformation chimique n’a pas encore eu lieu, les réactifs n’ont
   pas donc encore réagi. Les quantités initiales de réactifs sont
   notées :math:`n(A)_i` et :math:`n(B)_i` et les quantités initiales de
   produit sont notées :math:`n(C)_i` et :math:`n(D)_i`. Généralement,
   les produits ne sont pas encore formés, leur quantité de matière est
   nulle

-  la ligne :math:`\ell_4` indique le déroulement de la transformation
   au cours du temps. Les quantités des diverses espèces évoluent
   proportionnellement à leur nombre stœchiométrique et à l’avancement
   :math:`x` de la réaction.

   Quand :math:`a` moles de :math:`A` réagissent, :math:`b` moles de
   :math:`B` réagissent pour former :math:`c` moles de :math:`C` et
   :math:`d` moles de :math:`D`.

   Quand :math:`a \times x` moles de :math:`A` réagissent,
   :math:`b \times x` moles de :math:`B` réagissent pour former
   :math:`c \times x` moles de :math:`C` et :math:`d \times x` moles de
   :math:`D`.

.. admonition:: Règle 1
   :class: note

   Les réactifs sont consommés, donc leur quantité de matière diminue. La
   quantité de réactif restante est égale à :
   sa quantité initiale moins le produit du nombre stoechiométrique du
   réactif par l’avancement :math:`x`.

   :math:`n(A) = n(A)_i - ax` et :math:`n(B) = n(B)_i - bx`.

.. admonition:: Règle 2
   :class: note

   Les produits se forment, donc leur quantité de matière augmente.
   La quantité de produit formée est égale à :
   sa quantité initiale plus le produit du nombre stoechiométrique du
   réactif par l’avancement :math:`x`.

   :math:`n(C) = n(C)_i + cx` et :math:`n(D) = n(D)_i + dx`.

-  la ligne :math:`\ell_5`, elle, indique les quantités des espèces
   chimiques à l’état final (E.F.) de la transformation chimique lorsque
   l’un des réactifs a été totalement consommé. L’avancement vaut alors
   :math:`x_{\text{max}}`.

Déterminer l’avancement maximal et le réactif limitant
------------------------------------------------------

À l’état final (E.F.), l’un des réactifs est totalement consommé (sa
quantité de matière est alors nulle).

-  Si :math:`A` est le réactif limitant alors
   :math:`n(A)_f = n(A)_i - a x_{\text{max}} = 0`, soit
   :math:`\displaystyle x_{\text{max}} = \frac{n(A)}{a}`;

-  Si :math:`B` est le réactif limitant alors
   :math:`n(B)_f = n(B)_i - b x_{\text{max}} = 0`, soit
   :math:`\displaystyle x_{\text{max}} = \frac{n(B)}{b}`;

.. admonition:: Règle 3
   :class: note

   L’avancement maximal :math:`x_{\text{max}}` correspond à la
   plus petite valeur.
   Le réactif limitant est celui qui vient à manquer le premier,
   c’est donc celui qui donne la plus petite valeur de :math:`x_{\text{max}}`.

Faire un bilan de matière à l’état final
----------------------------------------

Lorsque :math:`x_{\text{max}}` est déterminé, on peut alors
calculer les quantités de matière à l’état final (ligne :math:`\ell_5`).
