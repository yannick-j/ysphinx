Fiche méthode 6 : Nommer les molécules organiques, alcanes et alcools
=====================================================================

Nommer un alcane linéaire
-------------------------

Le nom d’un alcane linéaire est composé de deux parties: un **radical**
qui indique le nombre d’atome de carbone de la chaîne principale et le
suffixe « **-ane** ».

.. table:: Les principaux radicaux
   :align: center

   =============================== ==== === ==== === ==== === ==== ===
   Nombres d’atomes de carbone (C) 1    2   3    4   5    6   7    8
   =============================== ==== === ==== === ==== === ==== ===
   Radical                         méth éth prop but pent hex hept hex
   =============================== ==== === ==== === ==== === ==== ===

.. figure:: fig/ex-nom-alc-lin.svg
   :alt: Exemples de noms d'alcanes linéaires
   :align: center
   :target: ../../../_images/ex-nom-alc-lin.svg

   Exemples de noms d'alcanes linéaires

Nommer un alcane ramifié
------------------------

1. On recherche la chaîne carbonée principale qui est la plus longue
   possible et qui n’est pas forcément écrite en ligne droite.

.. figure:: fig/ex-rech-chain-carb-alc-ram-1.svg
   :alt: Exemple de chaîne carbonnée 1
   :align: center
   :target: ../../../_images/ex-rech-chain-carb-alc-ram-1.svg

   Exemple de chaîne carbonnée 1

.. figure:: fig/ex-rech-chain-carb-alc-ram-2.svg
   :alt: Exemple de chaîne carbonnée 2
   :align: center
   :target: ../../../_images/ex-rech-chain-carb-alc-ram-2.svg

   Exemple de chaîne carbonnée 2

2. On repère les ramifications. Ce sont les groupes alkyles, nommés avec
   le même radical que les alcanes linéaires mais avec le suffixe «
   **-yl** ».

+---------+-----------------+-----------------+-----------------+---+
| Formule | :math:`-\text{  | :math:`-\text{  | :math:`-\text{  | … |
|         | CH}_3`          | C}_2\text{H}_5` | C}_3\text{H}_7` |   |
+=========+=================+=================+=================+===+
| Nom     | méthyle         | éthyl           | propyle         | … |
+---------+-----------------+-----------------+-----------------+---+

.. figure:: fig/ex-ram-chain-carb-alc-ram-1.svg
   :alt: Exemple de ramification 1
   :align: center
   :target: ../../../_images/ex-ram-chain-carb-alc-ram-1.svg

   Exemple de ramification 1

.. figure:: fig/ex-ram-chain-carb-alc-ram-2.svg
   :alt: Exemple de ramification 2
   :align: center
   :target: ../../../_images/ex-ram-chain-carb-alc-ram-2.svg

   Exemple de ramification 2

3. On numérote la chaîne carbonée principale de telle sorte que les
   atomes de carbone portant les ramifications aient les plus petits
   indices.

.. figure:: fig/ex-num-chain-carb-alc-ram-1.svg
   :alt: Exemple de numérotation de la chaîne carbonée 1
   :align: center
   :target: ../../../_images/ex-num-chain-carb-alc-ram-1.svg

   Exemple de numérotation de la chaîne carbonée 1

.. figure:: fig/ex-num-chain-carb-alc-ram-2.svg
   :alt: Exemple de numérotation de la chaîne carbonée 2
   :align: center
   :target: ../../../_images/ex-num-chain-carb-alc-ram-2.svg

   Exemple de numérotation de la chaîne carbonée 2

4. On nomme la molécule: Le nom d’un alcane ramifié est composé des noms
   des groupes alkyles, nommés par ordre alphabétique sans le « e »
   final, précédés de leur indice de position, suivis du nom de l’alcane
   linéaire de même chaîne principale. Deux indices sont séparés par des
   virgules « , », un indice et une lettre par un tiret « - ».

   Lorsque le même groupe alkyle est présent plusieurs fois, on le nomme
   une seule fois en faisant précéder son nom des préfixes « di », « tri
   », « tétra »,… Les indices de position apparaissent autant de fois
   qu’il y a de groupe et sont séparés par une virgule.

.. figure:: fig/ex-nom-alc-ram-1.svg
   :alt: Exemple de nom d'alcane ramifié 1
   :align: center
   :target: ../../../_images/ex-nom-alc-ram-1.svg

   Exemple de nom d'alcane ramifié 1

.. figure:: fig/ex-nom-alc-ram-2.svg
   :alt: Exemple de nom d'alcane ramifié 2
   :align: center
   :target: ../../../_images/ex-nom-alc-ram-2.svg

   Exemple de nom d'alcane ramifié 2

Nommer un alcool
----------------

1. On repère la chaîne carbonée la plus longue comportant le **groupe
   caractéristique hydroxyle** :math:`-\text{OH}`.

2. On numérote la chaîne carbonée de telle sorte que l’atome de carbone
   qui porte le groupe hydroxyle :math:`-\text{OH}` (appelé carbone
   fonctionnel) ait le plus petit indice.

3. On repère les ramifications.

4. On nomme l’alcool en suivant les mêmes règles que pour les alcanes,
   en substituant le « e » final du suffixe « -ane » par le suffixe «
   -ol », précédé de l’indice de position du groupe hydroxyle.

.. figure:: fig/ex-nom-ol-1.svg
   :alt: Exemple de nom d'alcool 1
   :align: center
   :target: ../../../_images/ex-nom-ol-1.svg

   Exemple de nom d'alcool 1

.. figure:: fig/ex-nom-ol-2.svg
   :alt: Exemple de nom d'alcool 2
   :align: center
   :target: ../../../_images/ex-nom-ol-2.svg

   Exemple de nom d'alcool 2

