Fiche méthodes 2 : Conversions d'unités
=======================================

Tableau de conversion
---------------------

.. list-table:: Préfixes du Système international d'unités et nombres correspondants
   :align: center
   :header-rows: 1

   * - préfixe
     - symbole
     - puissance de dix
   * - téra
     - :math:`\text{T}`
     - :math:`10^{12}`
   * - giga
     - :math:`\text{G}`
     - :math:`10^{9}`
   * - méga
     - :math:`\text{M}`
     - :math:`10^{6}`
   * - kilo
     - :math:`\text{k}`
     - :math:`10^{3}`
   * - hecto
     - :math:`\text{h}`
     - :math:`10^{2}`
   * - déca
     - :math:`\text{da}`
     - :math:`10^{1}`
   * - (aucun)
     - (aucun)
     - :math:`10^{0}=1`
   * - déci
     - :math:`\text{d}`
     - :math:`10^{-1}`
   * - centi
     - :math:`\text{c}`
     - :math:`10^{-2}`
   * - milli
     - :math:`\text{m}`
     - :math:`10^{-3}`
   * - micro
     - :math:`\mu`
     - :math:`10^{-6}`
   * - nano
     - :math:`\text{n}`
     - :math:`10^{-9}`
   * - pico
     - :math:`\text{p}`
     - :math:`10^{-12}`

Exemples
   :math:`1~\text{nm} = 10^{-9}~\text{m}` :math:`\big(` on convertit des
   :math:`\text{nm}` en :math:`\text{m}` en multipliant par
   :math:`10^{-9} \big)`

   :math:`1~\text{m} = 10^{9}~\text{nm}` :math:`\big(` on convertit des
   :math:`\text{m}` en :math:`\text{nm}` en multipliant par
   :math:`10^9 \big)`

   :math:`1~\text{km} = 10^{3}~\text{m}` :math:`\big(` on convertit des
   :math:`\text{km}` en :math:`\text{m}` en multipliant par
   :math:`10^3 \big)`

   :math:`1~\text{m} = 10^{-3}~\text{km}` :math:`\big(` on convertit des
   :math:`\text{m}` en :math:`\text{km}` en multipliant par
   :math:`10^{-3} \big)`

.. Surfaces
.. --------

.. +-------+-------+-------+-------+-------+-------+-------+-------+-------+
.. | :math | .     | :ma   | .     | :mat  | .     | :ma   | .     | :mat  |
.. | :`\te |       | th:`\ |       | h:`\t |       | th:`\ |       | h:`\t |
.. | xt{da |       | text{ |       | ext{d |       | text{ |       | ext{m |
.. | m}^2` |       | m}^2` |       | m}^2` |       | m}^2` |       | m}^2` |
.. +=======+=======+=======+=======+=======+=======+=======+=======+=======+
.. | :mat  | :mat  | :mat  | :mat  | :mat  | :mat  | :mat  | :mat  | :mat  |
.. | h:`1` | h:`0` | h:`0` | h:`0` | h:`0` | h:`0` | h:`0` | h:`0` | h:`0` |
.. +-------+-------+-------+-------+-------+-------+-------+-------+-------+

.. list-table:: Unités au carré
   :align: center

   * - :math:`\text{dam}^2`
     - 
     - :math:`\text{m}^2`
     - 
     - :math:`\text{dm}^2`
     - 
     - :math:`\text{cm}^2`
     - 
     - :math:`\text{mm}^2`
     - 
   * - 
     - 
     - :math:`1`
     - :math:`0`
     - :math:`0`
     - 
     - 
     - 
     - 
     - 
   * - 
     - 
     - :math:`1`
     - :math:`0`
     - :math:`0`
     - :math:`0`
     - :math:`0`
     - 
     - 
     - 

Exemples:
   :math:`1~\text{m}^2 = 10~\text{dm} \times 10~\text{dm} = 100~\text{dm}^2`

   :math:`1~\text{m}^2 = 100~\text{cm} \times 100~\text{cm} = 10~000~\text{cm}^2 = 10^4~\text{cm}^2`

Volumes
-------

.. list-table:: Unités cubiques
   :align: center

   * - :math:`\text{m}^3`
     - 
     - 
     - :math:`\text{dm}^3`
     - 
     - 
     - :math:`\text{cm}^3`
     - 
     - 
     - :math:`\text{mm}^3`
     - 
     - 
   * - :math:`\text{kL}`
     - :math:`\text{hL}`
     - :math:`\text{daL}`
     - :math:`\text{L}`
     - :math:`\text{dL}`
     - :math:`\text{cL}`
     - :math:`\text{mL}`
     - 
     - 
     - 
     - 
     - 
   * - :math:`1`
     - :math:`0`
     - :math:`0`
     - :math:`0`
     - 
     - 
     - 
     - 
     - 
     - 
     - 
     - 
   * - 
     - 
     - 
     - :math:`1`
     - :math:`0`
     - :math:`0`
     - :math:`0`
     - 
     - 
     - 
     - 
     - 
   * - 
     - 
     - 
     - 
     - 
     - 
     - :math:`1`
     - 
     - 
     - 
     - 
     - 

Exemples:
   :math:`1~\text{m}^3 = 1~000~\text{L} = 10^3~\text{L}`

   :math:`1~\text{L} = 1~\text{dm}^3 = 10^3~\text{mL}`

   :math:`1~\text{mL} = 1~\text{cm}^3`

   :math:`1~\text{m}^3 = 1~\text{m} \times 1~\text{m} \times 1~\text{m}`

   :math:`= 10~\text{dm} \times 10~\text{dm} \times 10~\text{dm} = 1~000~\text{dm}^3`

   :math:`= 10^3~\text{dm}^3 = 10^3~\text{L}`

   :math:`= 100~\text{cm} \times 100~\text{cm} \times 100~\text{cm}`

   :math:`= 1~000~000~\text{cm}^3 = 10^6~\text{cm}^3 = 10^6~\text{mL}`

