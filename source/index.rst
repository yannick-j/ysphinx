.. sphinx documentation master file, created by
   sphinx-quickstart on Thu May 18 08:52:54 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation
=============

.. toctree::

   doc/CommonMark
   doc/MyST
   doc/book-theme
   doc/sphinx-design
   doc/ReStructuredText

Première spécialité
===================

Fiches
------

.. toctree::

   ps/fiches/alphabet-grec

Méthodes
^^^^^^^^

.. toctree::

   ps/fiches/méthodes/fm1-notascient-cs
   ps/fiches/méthodes/fm2-conv-unit
   ps/fiches/méthodes/fm3-redac-cr
   ps/fiches/méthodes/fm4-molecule
   ps/fiches/méthodes/fm5-tab-av
   ps/fiches/méthodes/fm6-nom-mol-orga

Techniques
^^^^^^^^^^

Notebooks
^^^^^^^^^

.. toctree::

   ipynb/c8bis

